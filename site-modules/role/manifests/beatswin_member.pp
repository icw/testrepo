class role::beatswin_member {
  include ::profile::base_windows
  include ::profile::dns::client
  include ::profile::consul::client
  include ::profile::sensu::agent_windows
}
