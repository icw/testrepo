class role::kibana_member {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::client
    include ::profile::kjappkibana::install
}
