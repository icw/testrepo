class role::elasticsearch_member {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::client
    include ::profile::elasticsearch::install
}
