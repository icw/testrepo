# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include kibana::install


class kibana::install {

  #included and used to get and extract kibana file
  include ::archive

  $source   = 'https://artifacts.elastic.co/downloads/kibana/kibana-7.9.2-linux-x86_64.tar.gz'
  $filename = 'kibana-7.9.2-linux-x86_64.tar.gz'

  file {'/opt/kibana':
    ensure => directory,
  }

  archive { 'kibana':
    path         => "/tmp/${filename}",
    extract      => true,
    extract_path => '/opt/kibana',
    source       => $source,
    creates      => "kibana",
  }

}
