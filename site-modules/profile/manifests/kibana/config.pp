# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include kibana::config
class kibana::config {
	
	#server-host
	#elasticsearch.host -> ip+port

	$file_hash = {
	elasticsearch_port => 9200,
	elasticsearch_ip   => "elasticsearch.node.consul", 	# this may be an option? 
	}


    file { '/etc/kibana/config/kibana.yml':
      ensure => file,
      content => epp('profile/kibana_config.epp', file_hash),
    }
		
}
