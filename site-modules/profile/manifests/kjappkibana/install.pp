class profile::kjappkibana::install ( 

String $ip_elasticsearch	= '192.168.180.163', 
String $port_server		= '5601',
String $port_elasticsearch	= '9200',
String $ip_kibana		= '192.168.180.156',
)
{
include ::apt


$kibana_hash = {
'ip_elasticsearch' => $ip_elasticsearch,
'port_server' => $port_server,
'port_elasticsearch' => $port_elasticsearch,
'ip_kibana' => $ip_kibana,
}

exec { 'apt-update1':
	command => '/usr/bin/apt-get update',
	notify => Package['java'],
}

# Add elasticsearch gpg key
apt::key { 'puppet gpg key':
    id     => '46095ACC8548582C1A2699A9D27D666CD88E42B4', 
    source => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
    notify => Exec['apt-update2'],
}

package { 'java':
        name => 'default-jre',
	ensure => present,
	provider => 'apt',
	notify => File['kibanaRepo'],
}

file { 'kibanaRepo':
	ensure => present,
	path => '/etc/apt/sources.list.d/elastic-7.x.list',
	content => 'deb https://artifacts.elastic.co/packages/7.x/apt stable main',
	notify => Package['kibanaPackage'],
}

exec { 'apt-update2':
	command => '/usr/bin/apt-get update',
	require => File['kibanaRepo'],
}

package { 'kibanaPackage':
        name => 'kibana',
	ensure => present,
	provider => 'apt',
	require => Exec['apt-update2'],
}



file { 'kibanaconf':
	ensure => present,
	path => '/etc/kibana/kibana.yml',
	content => epp("profile/kibana_config.yaml.epp",$kibana_hash),
        owner   => 'kibana',
        group   => 'kibana',
        mode    => '0660',
}
service { 'kibanaService':
	name => 'kibana',
	ensure => running,
	require => Package['kibanaPackage'],
}
}
