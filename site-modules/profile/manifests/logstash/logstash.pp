class profile::logstash::logstash ( 

String $ip_elasticsearch	= '192.168.180.163', 
String $port_beats		= '5044',
String $ip_logstash		= '192.168.180.179',

)
{
include ::apt


$logstash_hash = {
'ip_elasticsearch' => $ip_elasticsearch,
'port_beats' => $port_beats,
}

exec { 'apt-update1':
	command => '/usr/bin/apt-get update',
	notify => Package['java'],
}

# Add elasticsearch gpg key
apt::key { 'puppet gpg key':
    id     => '46095ACC8548582C1A2699A9D27D666CD88E42B4', 
    source => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
    notify => Exec['apt-update2'],
}

package { 'java':
        name => 'default-jre',
	ensure => present,
	provider => 'apt',
	notify => File['logstashRepo'],
}

file { 'logstashRepo':
	ensure => present,
	path => '/etc/apt/sources.list.d/elastic-7.x.list',
	content => 'deb https://artifacts.elastic.co/packages/7.x/apt stable main',
	notify => Package['logstashPackage'],
}

exec { 'apt-update2':
	command => '/usr/bin/apt-get update',
	require => File['logstashRepo'],
}

package { 'logstashPackage':
        name => 'logstash',
	ensure => present,
	provider => 'apt',
	require => Exec['apt-update2'],
}

file { 'logconf':
	ensure => present,
	path => '/etc/logstash/conf.d/logstash.conf',
	content => epp("profile/logstash_config.yaml.epp",$logstash_hash),
}
service { 'logstashService':
	name => 'logstash',
	ensure => running,
	require => Package['logstashPackage'],
}
}
