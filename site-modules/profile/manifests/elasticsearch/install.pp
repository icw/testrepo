class profile::elasticsearch::install(

String $port_http              = '9200',
) { 

$elastic_hash = {
'port_http' => $port_http,
}


include ::apt

# Add elasticsearch gpg key
apt::key { 'puppet gpg key':
    id     => '46095ACC8548582C1A2699A9D27D666CD88E42B4', 
    source => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
    notify => Exec['apt-update'],
}

package { 'apt-transport-httpsPackage':
        name => 'apt-transport-https',
	ensure => present,
	provider => 'apt',
}


# Java is bundled with elasticsearch in v.7
#package { 'java':
#         name => 'default-jre',
# 	ensure => present,
# 	provider => 'apt',
# 	notify => Package['elasticPackage'], 
#}

file { 'elasticRepo':
	ensure => present,
	path => '/etc/apt/sources.list.d/elastic-7.x.list',
	content => 'deb https://artifacts.elastic.co/packages/7.x/apt stable main',
}

exec { 'apt-update':
	command => '/usr/bin/apt-get update',
	require => [
	  File['elasticRepo'],
	  Package['apt-transport-httpsPackage'],
	],
}

package { 'elasticPackage':
        name => 'elasticsearch',
	ensure => present,
	provider => 'apt',
	require => Exec['apt-update'],
}

file { 'datadir':
        ensure => directory,
        path => '/data',
	owner => 'elasticsearch',
	group => 'elasticsearch',
	recurse => true,
	require => Package['elasticPackage'],
	notify => Service['elasticsearchService'],
}

file { 'logdir':
        ensure => directory,
        path => '/logs',
	owner => 'elasticsearch',
	group => 'elasticsearch',
	recurse => true,
	require => Package['elasticPackage'],
	notify => Service['elasticsearchService'],
}
file { 'elasticconf':
        ensure => present,
        path => '/etc/elasticsearch/elasticsearch.yml',
        content => epp("profile/elasticsearch_config.yaml.epp",$elastic_hash),
	notify => Service['elasticsearchService'],
}
service { 'elasticsearchService':
	name => 'elasticsearch',
	ensure => running,
}
}
