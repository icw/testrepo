# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::filebeat
class profile::filebeat::filebeat_init (
String $package_name          = 'filebeat',
String $package_ensure_linux  = '7.9.2-linux-x86_64',
String $ip_kibana             = '192.168.180.156',
String $port_kibana           = '5601',
String $ip_logstash           = '192.168.180.179',
String $port_logstash         = '5044',
String $download_source_linux = 'https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.2-linux-x86_64.tar.gz',
String $download_win          = 'https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.3-windows-x86_64.zip',
String $package_ensure_win    = '7.9.3-windows-x86_64',
){
include profile::filebeat::filebeat_install
include profile::filebeat::filebeat_config
include profile::filebeat::filebeat_service


}
