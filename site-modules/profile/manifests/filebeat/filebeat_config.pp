# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::filebeat_config
class profile::filebeat::filebeat_config {


  $ip_kibana      = $profile::filebeat::filebeat_init::ip_kibana
  $port_kibana    = $profile::filebeat::filebeat_init::port_kibana
  $ip_logstash    = $profile::filebeat::filebeat_init::ip_logstash
  $port_logstash  = $profile::filebeat::filebeat_init::port_logstash

  $filebeat_hash = {
    'ip_kibana'     => $ip_kibana,
    'port_kibana'   => $port_kibana,
    'ip_logstash'   => $ip_logstash,
    'port_logstash' => $port_logstash,
}

# Local vaiable
  $file_name = "${profile::filebeat::filebeat_init::package_name}-${profile::filebeat::filebeat_init::package_ensure_linux}"

  file {"/opt/filebeat/archive/${file_name}/filebeat.yml":
    ensure  => present,
    owner   => 'root',
    content => epp('profile/filebeat_linux.yaml.epp',$filebeat_hash),
}

}

