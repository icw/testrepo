# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::filebeat_service
class profile::filebeat::filebeat_service {


# Make a service for linux.
# Dependent on ..get_file & conifg

  service {'filebeat':
    ensure => running,
    enable => true,
    status => '/usr/sbin/service filebeat status | grep "is running"',
  }

  file {'/etc/systemd/system/filebeat.service':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0700',
    content => template('profile/filebeat_service_linux.erp'),
    notify  => Service['filebeat'],
  }
}
