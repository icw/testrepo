# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::filebeat_install
class profile::filebeat::filebeat_install {

include ::archive

# Class gets the package using puppet-archive, and extracts it.

# Using facter to get "os family name".
if $facts['os']['family'] == 'Debian' {

# Makes two directories where filebeat will be extracted.
# Makes the /opt look less crouded.
  file { [ '/opt/filebeat' , '/opt/filebeat/archive'] :
    ensure => directory,
  }

# Lockal variables.
  $archive_name  = "${profile::filebeat::filebeat_init::package_name}-${profile::filebeat::filebeat_init::package_ensure_linux}.tar.gz"
  $source_linux  =  $profile::filebeat::filebeat_init::download_source_linux
  $symbolic_link = "${profile::filebeat::filebeat_init::package_name}-${profile::filebeat::filebeat_init::package_ensure_linux}"

# Downloads and extracts filebeat.
  archive { $profile::filebeat::filebeat_init::package_name:
    source       => $source_linux,
    path         => "/tmp/${archive_name}",
    temp_dir     => '/tmp/',
    extract      => true,
    extract_path => '/opt/filebeat/archive',
    creates      => '/filebeat/archive',
    cleanup      => true,
  }

# Creates sym link that is used for running filbeat.service.
  file {'/usr/local/bin/filebeat':
    ensure => link,
    target => "/opt/filebeat/archive/${symbolic_link}/filebeat",
  }
}



if $facts['os']['family'] == 'Windows' {

# Creates two folders where filebeat will be extracted to.
  file{['C:\Windows\Temp\filebeat','C:\Windows\Temp\filebeat\archive']:
    ensure => directory,
  }

#Local variabels for Windows
  $source_win       = $profile::filebeat::filebeat_init::package_ensure_win
  $archive_name_win = "${profile::filebeat::filebeat_init::package_name}-${profile::filebeat::filebeat_init::package_ensure_win}"

  archive { $profile::filebeat::filebeat_init::package_name:
    source       => $source_win,
    path         => "C:\\Windows\\Temp\\${archive_name_win}",
    temp_dir     => 'C:\\Windows\\Temp',
    extract_path => "C:\\Windows\\Program\\sFiles\\archive",
    creates      => '\\filebeat\\archive',
    cleanup      => true,
  }
}

}
