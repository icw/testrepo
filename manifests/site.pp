node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}

node 'manager.node.consul' {
  include ::role::manager_server
}

node 'dir.node.consul' {
  include ::role::directory_server
}

node 'mon.node.consul' {
  include ::role::monitoring_server
}

node /elasticsearch\d?.node.consul/ {
  include ::role::elasticsearch_member
}

node /logstash\d?.node.consul/ {
  include ::role::logstash_member
}

node /kibana\d?.node.consul/{
  include ::role::kibana_member
}

node /beatswin\d?.node.consul/ {
  include ::role::beatswin_member
}

node /beatslin\d?.node.consul/ {
  include ::role::beatslin_member
}

node /redis\d?.node.consul/ {
  include ::role::redis_member
}

node /nginx\d?.node.consul/ {
  include ::role::nginx_member
}
